---
title: "Každý osmý kandidát do Senátu by Senát zrušil"
perex: "Ukazuje to vizualizace dat z volební kalkulačky. Porovnejte si před volbami názory kandidátů na trest smrti, EET, znárodnění Budvaru a dalších 32 ožehavých politických otázek."
description: "Porovnejte si před volbami názory kandidátů na trest smrti, EET, znárodnění Budvaru a dalších 32 ožehavých politických otázek."
authors: ["Petr Kočí"]
published: "26. září 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
socialimg: https://interaktivni.rozhlas.cz/data/volebni-kalkulacka-senat/www/media/socimg.png
url: "kalkulacka-senat"
libraries: ['https://interaktivni.rozhlas.cz/tools/d3/4.2.6.min.js', 'https://interaktivni.rozhlas.cz/tools/d3-queue/3.0.3.min.js']
recommended:
  - link: https://interaktivni.rozhlas.cz/historie-brno/
    title: Volební mapa Brna: dominance lidovců dokáže přežít sto let a dvě diktatury
    perex: Politická paměť některých městských částí je nečekaně dlouhá. Dnešní lokální podporu křesťanských demokratů lze vystopovat až do prvních voleb za Rakouska-Uherska. Podobné vzorce platí i pro komunisty.
    image: https://interaktivni.rozhlas.cz/historie-brno/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/pripojeni-nemusi-byt-k-dispozici/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Ahoj, jsem Tyranosaurus Cenzorex. Možná znáte mého příbuzného, který se objeví v prohlížeči, když se vám porouchá připojení k internetu.
    image: https://interaktivni.rozhlas.cz/pripojeni-nemusi-byt-k-dispozici/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/yusra/
    title: Yusra plave o život: Seriál, který od Českého rozhlasu převzala i BBC 
    perex: Uprchlická celebrita, olympijská plavkyně. Osmnáctiletá Yusra Mardini je nyní hvězdou v záři reflektorů. V srpnu 2015 však byla na útěku. Uprchla se svou sestrou Sarah hledat nový domov. Reportérka Magdalena Sodomková a fotograf Lam Duc Hien zachytili její putování přes Egejské moře do Evropy.
    image: https://interaktivni.rozhlas.cz/yusra/media/fb2.jpg
---

<aside class="big">
  <div id="container"></div>
</aside>

_Zdroj dat: [Volební kalkulačka](https://volebnikalkulacka.cz/) provozovaná sdružením [KohoVolit.eu](http://kohovolit.eu/cs/o-nas/). Analýza a vizualizace: [Český rozhlas](https://interaktivni.rozhlas.cz/)_

Skoro dvě třetiny ze všech, kdo po říjnových volbách chtějí zasednout v horní komoře Parlamentu, si  myslí, že [ministři vlády nemají vydávat noviny](http://www.rozhlas.cz/zpravy/politika/_zprava/ctyri-komentatori-ctyri-nazory-lex-babis-je-prazdny-marketing-pise-honzejk-ukazuje-na-hlubsi-problem-mini-sidlo--1652056) ani vlastnit jiná zpravodajská média.

Mezi pětadvaceti kandidáty hnutí ANO – jejichž šéf Andrej Babiš vlastní Mladou frontu Dnes, rádio Impuls či Lidové noviny – se našel jediný, který deklaruje, že by v této věci s nadřízeným nesouhlasil.

Podobně pevnou stranickou disciplínu je vidět například i v odpovědích kandidátů ANO na otázku, zda by zvedli ruku pro zrušení elektronické evidence tržeb (jejich koaliční partneři z ČSSD a KDU-ČSL to zdaleka tak jednoznačně nevidí), nebo v odpovědích potenciálních senátorů za ČSSD, zda má být zachována přímá volba prezidenta (_ano_), nebo jestli má Česká republika vystoupit z Evropské unie (_ne_). V této otázce se s nimi jednomyslně shodnou i všichni kandidáti ODS a lidovců.

## Co senátor, to čtvereček
Přehledně je to vidět v interaktivní vizualizaci, kterou najdete v úvodu článku. Stačí si v rozbalovacím menu nahoře vybrat téma a grafika ukáže, jak se k němu staví jednotliví uchazeči o senátorská křesla.

Každý čtvereček představuje jednoho kandidáta. Najeďte na něj myší nebo ťukněte prstem na dotykovou obrazovku a dozvíte se, který to je. Když na čtvereček kliknete nebo ťuknete prstem podruhé, můžete si poslechnout [rozhovor Českého rozhlasu s daným politikem](https://interaktivni.rozhlas.cz/kandidati-do-senatu/).

Rozděleni jsou podle volebních stran. Každá , která do voleb postavila alespoň dva kandidáty, má v grafu svůj vlastní řádek. Strany a koalice s jediným kandidátem jsou pro větší přehlednost sloučeny v posledních dvou řádcích.

Data shromáždilo sdružení [KohoVolit.eu](http://kohovolit.eu/). Na jejich základě pak vytvořilo aplikaci [Volební kalkulačka](https://volebnikalkulacka.cz/). Každý, kdo odpoví na stejnou sadu otázek, si v ní může nechat spočítat míru názorové shody se všemi kandidáty.

Analýza Českého rozhlasu nabízí jiný pohled na stejná data: ukazuje, jaké je rozložení názorů mezi všemi kandidáty a napříč stranami.   

## Sám se sebou souhlasí na 69 procent
Stejnou sadou otázek obeslali autoři Volební kalkulačky všech 233 kandidátů se žádostí, aby na každou odpověděli ANO, NE, nebo NEVÍM/JE MI TO JEDNO. Odpovědi dostali od 196 z nich. Procento kandidátů, kteří odpověděli (ke konci minulého týdne 84 procent), je letos podle autorů otázek rekordní. 

Kandidáti měli také možnost připojit ke každé odpovědi vysvětlující komentář. Ve vizualizaci se vám u těch, kteří té možnosti využili, zobrazí poté, co na jejich čtvereček ukážete myší. 

Jeden z kandidátů po zveřejnění Volební kalkulačky na sociální sítě napsal, že si ji zkusil vyplnit, ale sám se sebou měl názorovou shodu jen 69 procent.  

"Pan kandidát buď pozapomněl své vlastní odpovědi, nebo se někde ukliknul. Anebo je třetí možnost, že odpovědi mu připravoval někdo z jeho volebního týmu a on sám s tím nebyl úplně seznámen," [vysvětlil](http://video.aktualne.cz/dvtv/koho-volit-nase-kalkulacka-napovi-vetsina-kandidatu-je-proti/r~f1af06cc818b11e683920025900fea04/) jeden z autorů kalkulačky Jaroslav Bílek v rozhovoru pro DVTV.

"Nejvíc odpovědí _nevím_ mají kandidáti KSČM a ANO. Naproti tomu kandidáti TOP 09 a ODS mají nejčastěji jasný názor," všiml si také analytik.

Při zkoumání odpovědí ho zaujala také široká shoda kandidátů za strany vládní koalice i těch opozičních na tom, že kvóty EU na přerozdělení uprchlíků by mělo Česko odmítnout.

Vůbec nejširší shoda ze všech otázek zavládla nad tou, zda mají zůstat současné žákovské slevy na jízdné ve veřejné dopravě. Na jejich zachování se shodlo 190 kandidátů, tedy 81,5 procenta ze všech oslovených.





<aside class="big">
  <div id="container"></div>
</aside>